/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sicemal.categoriaproduto;

import com.sicemal.categoriaproduto.Cadastro;
import com.sicemal.categoriaproduto.Produto;
import com.sicemal.categoriaproduto.dao.ProdutoDAO;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author usuario
 */
public class Cadastro {
    
    public static void main(String[] args) {
        //Cadastro cadastro = new Cadastro();
        
        //cadastro.cadastraProdutos();
        //cadastro.cadastraCategorias();
        
	@SuppressWarnings("unused")
	SessionFactory sessionFactory;
        sessionFactory = new Configuration().configure().buildSessionFactory();
        
        
        System.out.println("Cadastros gerados com sucesso!");
    }
        
    
    public void cadastraProdutos()
    {
        String descricao[] = {"Bicicleta", "Televisão", "DVD" };
        Double preco[] = {356.83, 19.99, 195.60};
        Produto produto = new Produto();
        produto.setDescricao("Descricao");
        produto.setPreco(new BigDecimal("123.45"));
        ProdutoDAO produtoDAO = new ProdutoDAO();
        produtoDAO.salvar(produto);
        
    }
    
    public void cadastraCategorias()
    {
        
    }
    
}
