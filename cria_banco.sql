SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `categoria` ;
CREATE SCHEMA IF NOT EXISTS `categoria` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

SHOW WARNINGS;
USE `categoria` ;

-- -----------------------------------------------------
-- Table `categoria`.`PRODUTO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `categoria`.`PRODUTO` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `categoria`.`PRODUTO` (
  `cod_produto` INT NOT NULL AUTO_INCREMENT ,
  `descricao` VARCHAR(50) NOT NULL ,
  `preco` DECIMAL(14,2) NOT NULL ,
  PRIMARY KEY (`cod_produto`) ) 
DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
ENGINE = InnoDB;

SHOW WARNINGS;