/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sicemal.categoriaproduto;

import com.sicemal.categoriaproduto.Categoria;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.*;


/**
 *
 * @author usuario
 */
@Entity
@Table(name = "produto")
public class Produto implements Serializable {
    private static final long serialVersionUID = -344L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    @Id
    @GeneratedValue
    @Column(name = "cod_produto")
    private Integer produto;
    
    @Column(name = "descricao")
    private String descricao;
    
    @Column(name = "preco")
    private BigDecimal preco;

    /**
     * @return the produto
     */
    public Integer getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Integer produto) {
        this.produto = produto;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the preco
     */
    public BigDecimal getPreco() {
        return preco;
    }

    /**
     * @param preco the preco to set
     */
    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }
    
    
    
}
