/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sicemal.categoriaproduto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

import com.sicemal.categoriaproduto.Produto;


/**
 *
 * @author usuario
 */
@Entity
@Table(name = "categoria")
public class Categoria implements Serializable {
    private static final long serialVersionUID = 653L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    @Id
    @GeneratedValue
    @Column(name = "cod_categoria")
    private Integer categoria;
    
    @Column(name = "descricao")
    private String descricao;
    
    @ManyToMany
    @JoinTable(name = "categoria_produto", joinColumns = {@JoinColumn(name = "cod_catetoria")},
            inverseJoinColumns = {@JoinColumn(name = "cod_produto")})
    private Set<Produto> produtos = new HashSet<Produto>();

    /**
     * @return the categoria
     */
    public Integer getCategoria() {
        return categoria;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @return the produtos
     */
    public Set<Produto> getProdutos() {
        return produtos;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @param produtos the produtos to set
     */
    public void setProdutos(Set<Produto> produtos) {
        this.produtos = produtos;
    }
    
    
}
