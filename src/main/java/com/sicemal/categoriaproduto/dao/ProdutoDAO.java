/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sicemal.categoriaproduto.dao;

import com.sicemal.categoriaproduto.Produto;
import com.sicemal.conexao.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author usuario
 */
public class ProdutoDAO {
    private Session sessao;
    private Transaction transacao;
    public void salvar(Produto produto)
    {
        try {
            this.sessao = HibernateUtil.getSessionFactory();
            
            this.transacao = this.sessao.beginTransaction();
            this.sessao.save(produto);
            this.transacao.commit();
        }
        catch (HibernateException e) {
            System.out.println("Não foi possível inserir a categoria. Erro: " +
                    e.getMessage());
        }
        finally {
            try {
                if (this.sessao.isOpen())
                    this.sessao.close();
            }
            catch (Throwable e) {
                System.out.println("Erro ao fechar operação de inserção. Mensagem: " +
                        e.getMessage());
            }
        }
    }
    
    
}
